package com.jhonystein.pedidex.repository;

import com.jhonystein.pedidex.model.Regra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegraRepository extends JpaRepository<Regra, Long>{
    Regra findByRegra(String regra);
}
